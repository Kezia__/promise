const form = document.querySelector('.form')
const userName = document.querySelector('.form__name')
const userSurname = document.querySelector('.form__surname')
const submit = document.querySelector('.form__submit')

form.addEventListener('submit', (e) => e.preventDefault());

const userData = {
    name: '',
    surname: '',
}

const getUserData = () => {
    userData.name = userName.value
    userData.surname = userSurname.value
    promiseState()
}

const promiseState = () => {    
    const requestData = new Promise(function(response, reject){
    if(userData.name !== '' && userData.surname !== ''){
        response(userData)
    } else {
        reject(new Error('НИЧИВО НЕ ПАЛУЧИТСО'))
    }
})
    requestData.then(result => console.log(result)),error => console.log(error);
}

submit.addEventListener('click', getUserData)

